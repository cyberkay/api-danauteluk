var app = require('./shared');
var conf = require('./config');

class Server {
    constructor(){
    }

    init(){
        var serverListen = app.exp.listen(conf.port, function () {
            app.logger.info('Server listening on port ' + conf.port);
        });

        serverListen.on('error', function (err) {
            app.logger.error("Error handling");
        });
    }
}

exports.Server = new Server();
