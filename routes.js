var app = require('./shared');
var db = require('./database');
var { Articles } = require('./models/Articles');

class Routes {
    constructor(){}

    init(){
        app.exp.get('/', function (req, res) {
            res.send("Web Service Now Listening on port 9001");
        });
        app.exp.get('/download', function (req, res) {
            db.con.connect(function(err) {
                db.con.query("SELECT * FROM danautel_dbbaru.download", function (err, result, fields) {
                    if (err){
                        console.log(err);
                        res.send("Error Get Articles");
                    } else {
                        console.log(result);
                        res.json(result);
                    }                    
                });
            });
        });

        app.exp.post('/download/insert', function (req, res) {
            db.con.connect(function(err) {
              var sql = "INSERT INTO danautel_dbbaru.download (judul, berkas) VALUES ('" + req.body.judul + "', '" + req.body.berkas + "')";
              db.con.query(sql, function (err, result) {
                if (err){
                    res.send(err);
                    console.log(err);
                } else {
                    res.send("Insert data has been send and success");
                }
              });
            });
        });
    
    }
}

exports.Routes = new Routes();
