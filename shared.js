const express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');

var appexpress = express();
appexpress.use(cors());

appexpress.use(bodyParser.json()); // for parsing application/json
appexpress.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

exports.exp = appexpress;
exports.logger = require('winston');
